package com.example.javasdktest01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Javasdktest01Application {

    public static void main(String[] args) {
        SpringApplication.run(Javasdktest01Application.class, args);
    }

}
