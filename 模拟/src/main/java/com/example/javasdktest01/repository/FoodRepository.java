package com.example.javasdktest01.repository;
import com.example.javasdktest01.entity.Food;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodRepository extends JpaRepository<Food,Integer> {
}
