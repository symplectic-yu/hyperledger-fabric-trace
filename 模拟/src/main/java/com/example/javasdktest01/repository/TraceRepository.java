package com.example.javasdktest01.repository;

import com.example.javasdktest01.entity.Trace;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TraceRepository extends JpaRepository<Trace,Integer> {
}
