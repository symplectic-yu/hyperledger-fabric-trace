package com.example.javasdktest01.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Trace {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String traceability_id;     //溯源id
    private String name;                //商品名称
    private String date_of_manufacture; //生产日期
    private String manufactor;          //生产厂家
    private String telephone;           //生产厂家电话

    private String listing_date;        //上架日期
    private String courier_number;      //快递单号
    private String term_of_validity;    //有效期
    private String company;             //公司名称
    private String tel;                 //公司电话
    private String delivery_date;       //快递日期

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTraceability_id(String traceability_id) {
        this.traceability_id = traceability_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate_of_manufacture(String date_of_manufacture) {
        this.date_of_manufacture = date_of_manufacture;
    }

    public void setManufactor(String manufactor) {
        this.manufactor = manufactor;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setListing_date(String listing_date) {
        this.listing_date = listing_date;
    }

    public void setCourier_number(String courier_number) {
        this.courier_number = courier_number;
    }

    public void setTerm_of_validity(String term_of_validity) {
        this.term_of_validity = term_of_validity;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }



    public Integer getId() {
        return id;
    }

    public String getTraceability_id() {
        return traceability_id;
    }

    public String getName() {
        return name;
    }

    public String getDate_of_manufacture() {
        return date_of_manufacture;
    }

    public String getManufactor() {
        return manufactor;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getListing_date() {
        return listing_date;
    }

    public String getCourier_number() {
        return courier_number;
    }

    public String getTerm_of_validity() {
        return term_of_validity;
    }

    public String getCompany() {
        return company;
    }

    public String getTel() {
        return tel;
    }

    public String getDelivery_date() {
        return delivery_date;
    }


}
